#! /usr/bin/env python

import random

inside = 0.0
t = 1000000

for count in range(0,t):
	x = random.uniform(0,1)
	y = random.uniform(0,1)
	if x * x + y * y < 1:
		inside += 1

print inside/t*4
